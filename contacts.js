import React from 'react';
import { StyleSheet, Text, View, FlatList } from 'react-native';

export default class ContactsScreen extends React.Component {
    render() {
      const {navigate} = this.props.navigation;
      return (
        <View>
          <FlatList
            data={[
              { key: 'Abigail' },
              { key: 'Alexandra' },
              { key: 'Chloe' },
              { key: 'Donna' },
              { key: 'Diane' },
              { key: 'Dorothy' },
              { key: 'Fiona' },
              { key: 'Gabrielle' },
              { key: 'Grace' },
              { key: 'Hannah' },
              { key: 'Heather' },
              { key: 'Irene' },
              { key: 'Jane' },
              { key: 'Jessica' },
              { key: 'Katherine' },
              { key: 'Kylie' },
              { key: 'Lauren' },
              { key: 'Leah' },
              { key: 'Mary' },
              { key: 'Megan' },
            ]}
            renderItem={({ item }) =>
              <Text style={styles.item}
              onPress={() => navigate('Details', {name: item.key})}>{item.key}</Text>
            }
          />
        </View>
      )
    }
  }

  const styles = StyleSheet.create({
    toolbar: {
      padding: 20,
      backgroundColor: 'black'
    },
    toolbarText: {
      fontSize: 20,
      fontWeight: 'bold',
      color: 'white'
    },
    item: {
      padding: 20
    }
  })