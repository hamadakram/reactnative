import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import ContactsScreen from './contacts';
import DetailsScreen from './details';

const MainNavigator = createStackNavigator({
  Contacts: { screen: ContactsScreen },
  Details: { screen: DetailsScreen },
  initialRouteName: 'Contacts',
});

const App = createAppContainer(MainNavigator);



export default App;
