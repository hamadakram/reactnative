import React from 'react';
import { StyleSheet, Text, View, FlatList } from 'react-native';

export default class DetailsScreen extends React.Component {
    render() {
        const { params } = this.props.navigation.state;
        const name = params.name
        return <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
            <Text style={styles.title}>Welcome, {name}</Text>
        </View>;
    }
}

const styles = StyleSheet.create({
    title: {
        fontSize: 35,
        fontWeight: 'bold',
        color: 'black'
    }
})